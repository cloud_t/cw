package xyz.nodomain.codewars

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import xyz.nodomain.codewars.inject.DaggerAppComponent


class CwApp: DaggerApplication() {

    override fun onCreate() {
        super.onCreate()

        // todo app inits
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

}