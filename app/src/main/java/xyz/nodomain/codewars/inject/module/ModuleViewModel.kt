package xyz.nodomain.codewars.inject.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import xyz.nodomain.codewars.ui.search.UserSearchViewModel
import xyz.nodomain.codewars.util.ViewModelFactory
import xyz.nodomain.codewars.util.ViewModelKey


@Module
abstract class ModuleViewModel {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(UserSearchViewModel::class)
    abstract fun postUserViewModel(viewModel: UserSearchViewModel): ViewModel
}