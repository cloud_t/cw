package xyz.nodomain.codewars.inject.module

import android.app.Application
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import xyz.nodomain.codewars.CwApp
import javax.inject.Singleton


@Module
class ModuleWww (){
    val BASE_URL = "https://www.codewars.com/api/v1/"
    val AUTH_HEADER = "Authorization"
    val AUTH_KEY = "uKzjSy1J8XghdoS2b9Pz"

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideInterceptor(): Interceptor{
        return Interceptor { chain ->
            val url = chain
                .request()
                .url()
                .newBuilder()
                .build()

            val request = chain.request()
                .newBuilder()
                .url(url)
                .addHeader(AUTH_HEADER, AUTH_KEY)
                .build()

            chain.proceed(request)
        }
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(cache: Cache, interceptor: Interceptor): OkHttpClient {
        val client = OkHttpClient.Builder().addInterceptor(interceptor)
        client.cache(cache)
        return client.build()
    }

    @Provides
    @Singleton
    fun provideHttpCache(app: CwApp): Cache {
        val maxSize = 1024 * 1024 * 2L
        val cache = Cache(app.cacheDir, maxSize)
        return cache
    }




//    fun getRetrofit(vararg interceptors: Interceptor) = Retrofit.Builder()
//        .baseUrl("")
//        .client(makeHttpClient(interceptors))
//        .build()
//
//    private fun makeHttpClient(interceptors: Array<out Interceptor>) = OkHttpClient.Builder()
//        .connectTimeout(5, TimeUnit.SECONDS)
//        .readTimeout(5, TimeUnit.SECONDS)
//        .addInterceptor(headersInterceptor())
//        .apply { interceptors().addAll(interceptors) }
//        .build()
//
//    fun headersInterceptor() = Interceptor { chain ->
//        chain.proceed(chain.request().newBuilder()
//            .addHeader(AUTH_HEADER, AUTH_KEY)
//            .build())
//    }
}