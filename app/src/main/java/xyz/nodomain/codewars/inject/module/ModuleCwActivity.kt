package xyz.nodomain.codewars.inject.module

import dagger.Module

/**
 * Activity module
 */
@Module
class ModuleCwActivity