package xyz.nodomain.codewars.inject.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import xyz.nodomain.codewars.ui.CwActivity

@Module
internal abstract class ModuleActivityBuilders {

    @ContributesAndroidInjector(modules = [
        ModuleCwActivity::class,
        ModuleSearchFragBuilder::class])
    internal abstract fun bindCwActivity(): CwActivity
}