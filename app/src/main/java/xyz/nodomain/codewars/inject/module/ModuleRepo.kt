package xyz.nodomain.codewars.inject.module

import xyz.nodomain.codewars.data.server.CwApi
import xyz.nodomain.codewars.data.repo.CwUserRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ModuleRepo {
    @Singleton
    @Provides
    internal fun provideUserRepo(api: CwApi) = CwUserRepo(api)
}