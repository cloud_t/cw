package xyz.nodomain.codewars.inject.module

import android.content.Context
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import xyz.nodomain.codewars.CwApp

@Module
class ModuleApp() {
    @Provides
    @Singleton
    internal fun provideContext(application: CwApp): Context {
        return application
    }
}