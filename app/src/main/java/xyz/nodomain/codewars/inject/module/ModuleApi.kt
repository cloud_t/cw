package xyz.nodomain.codewars.inject.module

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import xyz.nodomain.codewars.data.server.CwApi
import javax.inject.Singleton

@Module
class ModuleApi {
    @Provides
    @Singleton
    fun providesCwApi(retrofit: Retrofit): CwApi {
        return retrofit.create<CwApi>(CwApi::class.java)
    }
}