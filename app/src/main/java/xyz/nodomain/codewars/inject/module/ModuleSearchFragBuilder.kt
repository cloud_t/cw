package xyz.nodomain.codewars.inject.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import xyz.nodomain.codewars.ui.search.UserSearchFragment

@Module
abstract class ModuleSearchFragBuilder {

    @ContributesAndroidInjector(modules = [ModuleSearchFrag::class])
    internal abstract fun contributeSearchFragment(): UserSearchFragment
}
