package xyz.nodomain.codewars.inject

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import xyz.nodomain.codewars.CwApp
import xyz.nodomain.codewars.inject.module.*
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ModuleApp::class,
        ModuleActivityBuilders::class,
        ModuleApi::class,
        ModuleRepo::class,
        ModuleWww::class,
        ModuleViewModel::class
    ]
)
interface AppComponent : AndroidInjector<CwApp> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<CwApp>()
}