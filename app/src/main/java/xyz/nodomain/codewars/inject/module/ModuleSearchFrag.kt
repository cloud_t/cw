package xyz.nodomain.codewars.inject.module

import android.support.v4.app.Fragment
import dagger.Binds
import dagger.Module
import xyz.nodomain.codewars.ui.search.UserSearchFragment


@Module
abstract class ModuleSearchFrag {
    @Binds
    internal abstract fun bindFragment(fragment: UserSearchFragment): Fragment
}