package xyz.nodomain.codewars.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Ranks(
    val overall : Rank = Rank(),
    val languages : Map<String, Rank> = emptyMap()
)
