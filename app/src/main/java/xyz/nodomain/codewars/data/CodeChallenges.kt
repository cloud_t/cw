package xyz.nodomain.codewars.data

data class CodeChallenges(
    val totalAuthored : Int = 0,
    val totalCompleted : Int = 0
)
