package xyz.nodomain.codewars.data


data class Rank(
    val value : Int = 0,
    val name : String = "",
    val color : String = "",
    val score : Int = 0
)