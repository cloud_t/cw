package xyz.nodomain.codewars.data.server

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import xyz.nodomain.codewars.data.User

interface CwApi {
    @GET("users/{username}")
    fun fetch(@Path("username") username : String) : Call<User>
}