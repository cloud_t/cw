package xyz.nodomain.codewars.data.repo

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.nodomain.codewars.data.User
import xyz.nodomain.codewars.data.server.CwApi
import xyz.nodomain.codewars.util.BaseException
import xyz.nodomain.codewars.ui.Resource
import xyz.nodomain.codewars.util.RankedUser
import java.util.logging.Logger
import javax.inject.Inject

class CwUserRepo @Inject constructor(val api: CwApi) {

    private var fetchHistory : AutoRemoveMap = AutoRemoveMap()
    var historyData: MutableLiveData<List<RankedUser>> = MutableLiveData()

    fun doFetch(q: String): LiveData<Resource<User>> {
        val data = MutableLiveData<Resource<User>>()

        api.fetch(q).enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>?, response: Response<User>?) {
                val user = response?.body()

                if (user != null) {
                    val languageRank = user.ranks.languages.entries.sortedBy { it.value.score }.first()
                    val rankedUser = RankedUser(user, languageRank.key, languageRank.value.score)
                    cacheUser(rankedUser)
                    data.postValue(Resource.success(response.body()))
                } else {
                    data.postValue(Resource.error(BaseException(Throwable("not found?"))))
                }

            }

            override fun onFailure(call: Call<User>?, t: Throwable?) {
                // TODO fall back to offline data, if any, maybe also WARN UI of failure
                Logger.getLogger(CwUserRepo::class.java.name).warning("error: " + t.toString())
                val exception = BaseException(t)
                data.postValue(Resource.error(exception))
            }
        })

        return data
    }

    private fun cacheUser(rankedUser : RankedUser?) {
        if (!fetchHistory.containsKey(rankedUser?.user?.username)) {
            rankedUser?.user?.username?.let {
                fetchHistory.put(it, rankedUser)
                historyData.postValue(fetchHistory.values.reversed())
            }
        } else {
            // todo update
        }
    }

    class AutoRemoveMap : LinkedHashMap<String, RankedUser>() {
        private val capacity = 5

        override fun removeEldestEntry(eldest: MutableMap.MutableEntry<String, RankedUser>?): Boolean {
            return this.size > capacity
        }
    }
}
