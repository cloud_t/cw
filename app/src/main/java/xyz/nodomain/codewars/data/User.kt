package xyz.nodomain.codewars.data

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "cwusers", primaryKeys = ["username"])
data class User(
    val username: String = "<not found>",
    val name: String = "<not found>",
    val honor: Int = 0,
    val clan: String = "",
    val leaderboardPosition: Int = 0,
    val skills: List<String> = listOf(),

    @field:Embedded
    val ranks : Ranks = Ranks(),

    @field:Embedded
    val codeChallenges: CodeChallenges = CodeChallenges()
) {
    override fun toString(): String {
        return "$username - $name"
    }

}
