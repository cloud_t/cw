package xyz.nodomain.codewars.util

class BaseException(val exception: Throwable?): Exception();