package xyz.nodomain.codewars.util

import xyz.nodomain.codewars.data.User

data class RankedUser(val user: User, val language: String = "", val score: Int = 0)