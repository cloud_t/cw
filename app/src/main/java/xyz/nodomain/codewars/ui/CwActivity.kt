package xyz.nodomain.codewars.ui

import xyz.nodomain.codewars.R
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity
import xyz.nodomain.codewars.CwApp
import xyz.nodomain.codewars.ui.search.UserSearchFragment

class CwActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cw)

        savedInstanceState ?: supportFragmentManager.beginTransaction().add(R.id.content,
            UserSearchFragment.newInstance()).commit()
    }

}
