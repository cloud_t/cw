package xyz.nodomain.codewars.ui.search

import android.arch.lifecycle.*
import xyz.nodomain.codewars.data.repo.CwUserRepo
import xyz.nodomain.codewars.util.RankedUser
import javax.inject.Inject


class UserSearchViewModel @Inject constructor(private val repository: CwUserRepo): ViewModel(){

    val fetchInput: MutableLiveData<String> = MutableLiveData()
    val fetchResult = Transformations.switchMap(fetchInput) {repository.doFetch(it)}

    val honorSortCheck: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = false
    }

    val fetchHistory = repository.historyData

    val fetchHistorySorted = MediatorLiveData<List<RankedUser>>().apply {
        addSource(fetchHistory) {
            postValue(fetchHistory.value?.let { list ->
                if (honorSortCheck.value == true)
                    list.sortedByDescending { it.user.honor }
                else
                    list
            })
        }

        addSource(honorSortCheck) {
            postValue(fetchHistory.value?.let { list ->
                if(honorSortCheck.value == true)
                    list.sortedByDescending { it.user.honor }
                else
                    list
            })
        }
    }

    fun fetch(term: String){
        fetchInput.value = (term)
    }

}