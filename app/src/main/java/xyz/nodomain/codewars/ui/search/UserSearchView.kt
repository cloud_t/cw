package xyz.nodomain.codewars.ui.search

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.DataBindingUtil.inflate
import android.view.LayoutInflater
import android.view.View
import xyz.nodomain.codewars.BR
import xyz.nodomain.codewars.R
import xyz.nodomain.codewars.data.User
import xyz.nodomain.codewars.databinding.FragmentSearchBinding
import xyz.nodomain.codewars.util.RankedUser

class UserSearchView(inflater: LayoutInflater, listener: Listener) {

    var rootView: View? = null

    var binding: FragmentSearchBinding
    var historyAdapter : HistoryAdapter

    private var model: Model
    private var eventHandler: EventHandler

    init {
        model = Model()
        eventHandler = EventHandler(listener);

        binding = inflate(inflater, R.layout.fragment_search, null, false);
        binding.model = model
        binding.handler = eventHandler
        binding.searchEt.setHint(R.string.username_hint)
        binding.progress.visibility = View.INVISIBLE

        historyAdapter = HistoryAdapter()
        binding.historyRecycler.adapter = historyAdapter

        rootView = binding.root;
    }


    fun populateData(user: User) {
        model.name = user.username;
        model.clan = user.clan;
    }


    fun toggleLoading(isLoading : Boolean) {
        if (isLoading)
            binding.progress.visibility = View.VISIBLE
        else
            binding.progress.visibility = View.INVISIBLE
    }

    fun submitList(list: List<RankedUser>) {
        historyAdapter.submitList(list)
    }

    class Model : BaseObservable() {
        @Bindable
        var name: String? = ""
            set(value) {
                if (value == null) {
                    field = "no name"
                } else {
                    field = value
                }

                notifyPropertyChanged(BR.name)
            }
        @Bindable
        var clan: String? = ""
            set(value) {
                if (value == null) {
                    field = "no clan"
                } else {
                    field = value
                }

                notifyPropertyChanged(BR.clan)
            }
    }

    interface Listener {
        fun fetch(newValue: String)
    }

    class EventHandler(val listener: Listener)  {
        fun fetch(text : String) {
            listener.fetch(text)
        }
    }

}