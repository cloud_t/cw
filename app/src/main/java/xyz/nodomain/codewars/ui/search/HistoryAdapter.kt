package xyz.nodomain.codewars.ui.search

import android.databinding.DataBindingUtil
import android.support.v7.recyclerview.extensions.AsyncListDiffer
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import xyz.nodomain.codewars.R
import xyz.nodomain.codewars.databinding.HistoryListItemBinding
import xyz.nodomain.codewars.util.ListRecyclerAdapter
import xyz.nodomain.codewars.util.RankedUser

class HistoryAdapter(/*val onClick: (User) -> Unit*/) : ListRecyclerAdapter<RankedUser, HistoryListItemBinding>() {

    override val differ: AsyncListDiffer<RankedUser> =
        AsyncListDiffer(this, UserDiffCallback())

    override fun bind(binding: HistoryListItemBinding, item: RankedUser) {
        binding.rankedUser = item
    }

    override fun createBinding(parent: ViewGroup): HistoryListItemBinding {
        val layoutInflater = LayoutInflater.from(parent.context)
        return DataBindingUtil.inflate(
            layoutInflater,
            R.layout.history_list_item,
            parent,
            false
        )
    }


    class UserDiffCallback : DiffUtil.ItemCallback<RankedUser>() {
        override fun areItemsTheSame(oldItem: RankedUser, newItem: RankedUser) =
            newItem.user.username == oldItem.user.username

        override fun areContentsTheSame(oldItem: RankedUser, newItem: RankedUser) = newItem == oldItem
    }

}