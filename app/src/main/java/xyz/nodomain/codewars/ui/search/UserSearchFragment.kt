package xyz.nodomain.codewars.ui.search

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import dagger.android.support.DaggerFragment
import xyz.nodomain.codewars.data.User
import xyz.nodomain.codewars.ui.Resource
import xyz.nodomain.codewars.util.RankedUser
import javax.inject.Inject


class UserSearchFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var searchView: UserSearchView

    companion object {
        fun newInstance(): UserSearchFragment {
            return UserSearchFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val userSearchViewModel =
            ViewModelProviders.of(this, viewModelFactory)[UserSearchViewModel::class.java]


        searchView = UserSearchView(layoutInflater, object: UserSearchView.Listener{
            override fun fetch(newValue: String) {
                searchView.toggleLoading(true)
                userSearchViewModel.fetch(newValue)
            }

        })

        userSearchViewModel.fetchHistorySorted.observe(this, Observer<List<RankedUser>> { list ->
            list?.let { searchView.submitList(list) }
        })


        userSearchViewModel.fetchResult?.observe(this, Observer<Resource<User>> { resource ->
            if (resource != null && resource.status == Resource.Status.SUCCESS) {

                val user = resource.data
                if (user != null) {
                    searchView.populateData(user)
                } else {
                    searchView.populateData(User())
                }


            } else {
                searchView.populateData(User())
            }

            searchView.toggleLoading(false)
            hideKeyboard()
        })


        return searchView.binding.apply {
                setLifecycleOwner(this@UserSearchFragment)
                viewModel = userSearchViewModel
            }.root
    }

    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(if (currentFocus == null) View(this) else currentFocus)
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}